<?php

namespace Qandidate\Bundle\HangmanBundle\Tests\Controller;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Qandidate\Bundle\HangmanBundle\Entity\Game;
use Qandidate\Bundle\ToolBundle\Api\ApiTestCase;

class GameControllerTest extends ApiTestCase
{
    /**
     * Tests that the api returns a created response
     * @group game
     */
    public function testStart()
    {
        $response = $this->request('POST', 'qandidate_hangman_game_start');
        $this->assertCreatedResponse($response);
    }

    /**
     * Tests that the api returns a successful response
     * @group game
     */
    public function testAll()
    {
        $this->loadFixtures(array('Qandidate\Bundle\HangmanBundle\DataFixtures\ORM\LoadGameData'), null, 'doctrine', ORMPurger::PURGE_MODE_TRUNCATE);

        $response = $this->request('GET', 'qandidate_hangman_game_all');
        $this->assertSuccessfulResponseWithManyItems($response, $this::getGameKeys(), array(), 3);

        $data = $this->decode($response, 'id');

        return $data;
    }

    /**
     * Tests that the api returns a successful response
     * @group game
     */
    public function testOne()
    {
        $this->loadFixtures(array('Qandidate\Bundle\HangmanBundle\DataFixtures\ORM\LoadGameData'), null, 'doctrine', ORMPurger::PURGE_MODE_TRUNCATE);

        foreach(range(1,3) as $id)
        {
            $response = $this->request('GET', 'qandidate_hangman_game_one', array('id' => $id));
            $this->assertSuccessfulResponseWithOneItem($response, $this::getGameKeys());
        }
    }

    /**
     * Tests that user can win the game and cannot post after the game is won
     * @group game
     */
    public function testWinGame()
    {
        $this->loadFixtures(array('Qandidate\Bundle\HangmanBundle\DataFixtures\ORM\LoadGameData'), null, 'doctrine', ORMPurger::PURGE_MODE_TRUNCATE);

        $s = array('q','a','n','d','i','t','f','e'); # Sequence
        $h = array( 8,  6,  5,  3,  2,  1,  1,  0 ); # Number of hidden chars
        $t = array(11, 11, 11, 11, 11, 11, 10, 10 ); # Number of tries left

        for($i=0,$l=count($s); $i<$l; $i++)
        {
            $response = $this->request('POST', 'qandidate_hangman_game_guess', array('id' => 1), array('char' => $s[$i]));
            $this->assertSuccessfulResponseWithOneItem($response);

            $word  = $this->decode($response, 'word');
            $tries = $this->decode($response, 'tries_left');

            $this->assertEquals($h[$i], substr_count($word, Game::HIDDEN_CHAR));
            $this->assertEquals($t[$i], $tries);
        }

        $response = $this->request('POST', 'qandidate_hangman_game_guess', array('id' => 1), array('char' => 'a'));
        $this->assertForbiddenResponse($response);

        $status = $this->decode($response, 'status');
        $this->assertEquals(Game::STATUS_SUCCESS, $status);
    }

    /**
     * Tests that user can win the game and cannot post after the game is lost
     * @group game
     */
    public function testLoseGame()
    {
        $this->loadFixtures(array('Qandidate\Bundle\HangmanBundle\DataFixtures\ORM\LoadGameData'), null, 'doctrine', ORMPurger::PURGE_MODE_TRUNCATE);

        $s = array('m','r','g','q','i','t','t','c','j','k','p','a','z','y','x'); # Sequence
        $h = array( 9,  9,  9,  8,  7,  6,  6,  6,  6,  6,  6,  4,  4,  4,  4 ); # Number of hidden chars
        $t = array(10,  9,  8,  8,  8,  8,  7,  6,  5,  4,  3,  3,  2,  1,  0 ); # Number of tries left

        for($i=0,$l=count($s); $i<$l; $i++)
        {
            $response = $this->request('POST', 'qandidate_hangman_game_guess', array('id' => 1), array('char' => $s[$i]));
            $this->assertSuccessfulResponseWithOneItem($response);

            $word  = $this->decode($response, 'word');
            $tries = $this->decode($response, 'tries_left');

            $this->assertEquals($h[$i], substr_count($word, Game::HIDDEN_CHAR));
            $this->assertEquals($t[$i], $tries);
        }

        $response = $this->request('POST', 'qandidate_hangman_game_guess', array('id' => 1), array('char' => 'a'));
        $this->assertForbiddenResponse($response);

        $status = $this->decode($response, 'status');
        $this->assertEquals(Game::STATUS_FAIL, $status);
    }

    /**
     * Tests that the api returns a forbidden response
     * @group game
     */
    public function testBadChar()
    {
        $this->loadFixtures(array('Qandidate\Bundle\HangmanBundle\DataFixtures\ORM\LoadGameData'), null, 'doctrine', ORMPurger::PURGE_MODE_TRUNCATE);

        $badChars = array('am', '.', 1);

        foreach($badChars as $char)
        {
            $response = $this->request('POST', 'qandidate_hangman_game_guess', array('id' => 1), array('char' => $char));
            $this->assertForbiddenResponse($response);
        }

        $response = $this->request('POST', 'qandidate_hangman_game_guess', array('id' => 1));
        $this->assertForbiddenResponse($response);
    }

    /**
     * Tests that the api returns a 404 response
     * @group game
     */
    public function testGameNotFound()
    {
        $this->loadFixtures(array('Qandidate\Bundle\HangmanBundle\DataFixtures\ORM\LoadGameData'), null, 'doctrine', ORMPurger::PURGE_MODE_TRUNCATE);

        $response = $this->request('GET', 'qandidate_hangman_game_one', array('id' => 10), array('char' => 'a'));
        $this->assertResourceNotFoundResponse($response);

        $response = $this->request('POST', 'qandidate_hangman_game_guess', array('id' => 10), array('char' => 'a'));
        $this->assertResourceNotFoundResponse($response);
    }

    # Game keys

    static function getGameKeys()
    {
        return array('id', 'word', 'status', 'tries_left');
    }
}
