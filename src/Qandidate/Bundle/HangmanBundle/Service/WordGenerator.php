<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 31/10/14
 * Time: 17:12
 */

namespace Qandidate\Bundle\HangmanBundle\Service;

use SplFileObject;
use Symfony\Component\HttpKernel\Config\FileLocator;

/**
 * Class WordGenerator
 */
class WordGenerator
{
    /**
     * @var string
     */
    protected $filename;

    public function __construct(FileLocator $fileLocator)
    {
        $this->filename = $fileLocator->locate('@QandidateHangmanBundle/Resources/words/words.english');
    }

    /**
     * This method does not read the entire file into the memory
     * @return int
     */
    private function getNumberOfLines()
    {
        $fh = fopen($this->filename, 'rb');
        $lines = 0;

        while (!feof($fh)) {
            $lines += substr_count(fread($fh, 8192), "\n");
        }

        fclose($fh);

        return $lines;
    }

    public function getRandomWord()
    {
        $desiredLine = mt_rand(0, $this->getNumberOfLines()-1);
        $file = new SplFileObject($this->filename);
        $file->seek($desiredLine);
        return str_replace("\n", '', $file->current());
    }
} 