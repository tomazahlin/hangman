<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 21/05/14
 * Time: 17:09
 */

namespace Qandidate\Bundle\HangmanBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Qandidate\Bundle\HangmanBundle\Entity\Game;

class LoadGameData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $words = array('Qandidate', 'Test', 'Yes');

        foreach($words as $word)
        {
            $game = new Game();
            $game->setWordOriginal($word);

            $manager->persist($game);
            $manager->flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 100;
    }
}