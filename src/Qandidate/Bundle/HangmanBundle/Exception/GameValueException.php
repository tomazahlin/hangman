<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 31/10/14
 * Time: 23:51
 */

namespace Qandidate\Bundle\HangmanBundle\Exception;

class GameValueException extends \UnexpectedValueException
{
    function __construct($message, $statusCode=403)
    {
        $this->message=$message;
        $this->code=$statusCode;
    }
} 