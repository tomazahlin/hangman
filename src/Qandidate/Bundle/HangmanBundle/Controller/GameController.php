<?php

namespace Qandidate\Bundle\HangmanBundle\Controller;

use Qandidate\Bundle\HangmanBundle\Entity\Game;
use Qandidate\Bundle\HangmanBundle\Exception\GameValueException;
use Qandidate\Bundle\ToolBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class GameController extends AbstractController
{
    /**
     * @return Response
     */
    public function startAction()
    {
        $game = new Game();
        $this->persist($game);
        return $this->serialize($game, array(), Response::HTTP_CREATED);
    }

    /**
     * @return Response
     */
    public function allAction()
    {
        $games = $this->getRepository('QandidateHangmanBundle:Game')->findAll();
        return $this->serialize($games, array(), Response::HTTP_OK);
    }

    /**
     * @param $id
     * @return Response
     */
    public function oneAction($id)
    {
        $game = $this->getRepository('QandidateHangmanBundle:Game')->findOneBy(array('id' => $id));
        return $this->serialize($game, array(), Response::HTTP_OK);
    }

    /**
     * @param $id
     * @return Response
     */
    public function guessAction($id)
    {
        $validator = $this->get('qandidate_hangman.game_guess_validator');

        if($validator->validate())
        {
            $game = $this->getRepository('QandidateHangmanBundle:Game')->findOneBy(array('id' => $id));

            try
            {
                $game->guess($validator->getChar());
                $this->persist($game);
                return $this->serialize($game, array(), Response::HTTP_OK);
            }
            catch(GameValueException $e)
            {
                return $this->serialize($game, array(), Response::HTTP_FORBIDDEN);
            }
        }
        return $this->serialize(array(), array(), Response::HTTP_FORBIDDEN);
    }
}
