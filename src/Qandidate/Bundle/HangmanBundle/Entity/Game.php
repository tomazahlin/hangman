<?php

namespace Qandidate\Bundle\HangmanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\VirtualProperty;
use Qandidate\Bundle\HangmanBundle\Exception\GameValueException;

/**
 * Game
 * @ExclusionPolicy("none")
 */
class Game
{
    const INITIAL_TRIES = 11;

    const STATUS_SUCCESS = 'success';
    const STATUS_BUSY    = 'busy';
    const STATUS_FAIL    = 'fail';

    const HIDDEN_CHAR    = '.';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $word;

    /**
     * @var string
     */
    private $status;

    /**
     * @var integer
     */
    private $triesLeft;

    /**
     * @Exclude
     * @var string
     */
    private $wordOriginal;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set word
     * Automatically converts a word to dots
     * @param string $word
     * @return Game
     */
    public function setWord($word)
    {
        $this->word = preg_replace('@.@',$this::HIDDEN_CHAR,$word);

        return $this;
    }

    /**
     * Get word
     *
     * @return string 
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     *
     */
    public function isWordComplete()
    {
        return 0 === substr_count($this->getWord(), $this::HIDDEN_CHAR);
    }

    /**
     * Set status
     *
     * @param string $status
     * @throws \InvalidArgumentException
     * @return Game
     */
    public function setStatus($status)
    {
        if (!in_array($status, array(self::STATUS_SUCCESS, self::STATUS_BUSY, self::STATUS_FAIL))) {
            throw new \InvalidArgumentException("Invalid game status.");
        }

        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set triesLeft
     *
     * @param integer $triesLeft
     * @return Game
     */
    public function setTriesLeft($triesLeft)
    {
        $this->triesLeft = $triesLeft;

        return $this;
    }

    public function decreaseTriesLeft()
    {
        $this->triesLeft--;

        if($this->triesLeft < 0)
        {
            $this->triesLeft = 0;
        }
    }

    /**
     * Get triesLeft
     *
     * @return integer 
     */
    public function getTriesLeft()
    {
        return $this->triesLeft;
    }

    /**
     * Set wordOriginal
     *
     * @param string $wordOriginal
     * @return Game
     */
    public function setWordOriginal($wordOriginal)
    {
        $this->wordOriginal = $wordOriginal;
        $this->setWord($wordOriginal);

        return $this;
    }

    /**
     * Get wordOriginal
     *
     * @return string 
     */
    public function getWordOriginal()
    {
        return $this->wordOriginal;
    }

    /**
     * @param $char
     * @return $this
     */
    public function guess($char)
    {
        $this->validate($char);

        $w = $this->getWordOriginal();
        $oldWord = $this->getWord();

        $found = false;

        for($i=0, $l=strlen($w); $i<$l; $i++)
        {
            $r = substr($w, $i, 1);
            if(strcasecmp($char, $r) == 0)
            {
                $found = true;
                $this->word = substr_replace($this->word, $r, $i, 1);
            }
        }

        if(!$found || $oldWord === $this->word)
        {
            $this->decreaseTriesLeft();
        }

        return $this;
    }

    /**
     * This simple validation is ok here, when more complex, it should go in a yaml file
     * @param $char
     * @return $this
     * @throws GameValueException
     */
    public function validate($char)
    {
        if (!preg_match('/^[a-zA-Z]$/', $char)) {
            throw new GameValueException('Character is not valid.');
        }

        if ($this->getStatus() === $this::STATUS_FAIL) {
            throw new GameValueException('Game is already finished (fail).');
        }

        if ($this->getStatus() === $this::STATUS_SUCCESS) {
            throw new GameValueException('Game is already finished (success).');
        }

        return $this;
    }
}
