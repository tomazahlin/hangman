<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 31/10/14
 * Time: 14:27
 */

namespace Qandidate\Bundle\HangmanBundle\Entity\Repository;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;

class GameRepository extends EntityRepository
{
    public function findOneBy(array $criteria)
    {
        $game = parent::findOneBy($criteria);

        if(!$game)
        {
            throw new EntityNotFoundException('Game not found!');
        }

        return $game;
    }
}