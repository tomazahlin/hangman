<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 06/05/14
 * Time: 15:33
 */

namespace Qandidate\Bundle\HangmanBundle\Entity\Listener;

use Qandidate\Bundle\HangmanBundle\Entity\Game;
use Qandidate\Bundle\HangmanBundle\Service\WordGenerator;

class GameListener
{
    /**
     * @var WordGenerator
     */
    protected $wordGenerator;

    public function __construct(WordGenerator $wordGenerator)
    {
        $this->wordGenerator = $wordGenerator;
    }

    public function prePersistHandler(Game $game)
    {
        if($game->getWord() === null)
        {
            $game->setWordOriginal($this->wordGenerator->getRandomWord());
        }

        if($game->getTriesLeft() === null)
        {
            $game->setTriesLeft($game::INITIAL_TRIES);
        }

        if($game->getStatus() === null)
        {
            $game->setStatus($game::STATUS_BUSY);
        }
    }

    public function preUpdateHandler(Game $game)
    {
        if($game->isWordComplete())
        {
            $game->setStatus($game::STATUS_SUCCESS);
        }
        elseif($game->getTriesLeft() === 0)
        {
            $game->setStatus($game::STATUS_FAIL);
        }

    }
}