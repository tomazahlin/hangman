<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 31/10/14
 * Time: 23:33
 */

namespace Qandidate\Bundle\HangmanBundle\Validator;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RequestStack;

class GameGuessValidator
{
    /**
     * @var ParameterBag
     */
    private $post;

    /**
     * @var string
     */
    private $char;

    public function __construct(RequestStack $requestStack)
    {
        $this->post = $requestStack->getCurrentRequest()->request;
    }

    public function validate()
    {
        if(!$this->post->has('char'))
        {
            return false;
        }

        $this->char = $this->post->get('char');

        if (!preg_match('/^[a-zA-Z]$/', $this->getChar())) {
            return false;
        }

        return true;
    }

    public function getChar()
    {
        return $this->char;
    }
} 