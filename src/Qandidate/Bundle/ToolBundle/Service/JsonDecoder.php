<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 24/06/14
 * Time: 11:52
 */

namespace Qandidate\Bundle\ToolBundle\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class JsonDecoder
{
    /**
     * @var Request
     */
    protected $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function onKernelRequest()
    {
        if($this->request->getMethod() == 'POST') {
            if (0 === strpos($this->request->headers->get('Content-Type'), 'application/json')) {
                $data = json_decode($this->request->getContent(), true);
                $this->request->request->replace(is_array($data) ? $data : array());
            }
        }
    }
} 