<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 31/10/14
 * Time: 20:32
 */

namespace Qandidate\Bundle\ToolBundle\Controller;


use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractController extends Controller
{
    /**
     * @param $entity
     * @param bool $flush
     */
    protected function persist($entity, $flush=true)
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($entity);

        if($flush)
        {
            $manager->flush($entity);
        }

        return;
    }

    /**
     * @param $data
     * @param $metaData
     * @param $statusCode
     * @throws \LogicException
     * @return Response
     */
    protected function serialize($data, $metaData, $statusCode)
    {
        $serializer = $serializer = $this->container->get('jms_serializer');

        $returnData = ['data' => $data];

        foreach($metaData as $key=>$value)
        {
            if($key === 'data')
            {
                throw new \LogicException('You cannot override data.');
            }
            $returnData[$key] = $value;
        }

        $response = new Response($serializer->serialize($returnData, 'json'), $statusCode);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Proxy method to get object repository
     * @param $className
     * @return ObjectRepository
     */
    protected function getRepository($className)
    {
        return $this->getDoctrine()->getManager()->getRepository($className);
    }
} 