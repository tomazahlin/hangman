<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 10/05/14
 * Time: 20:35
 */

namespace Qandidate\Bundle\ToolBundle\Api;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

abstract class ApiTestCase extends WebTestCase
{
    /**
     * @param $method
     * @param $routeId
     * @param array $routeParams
     * @param array $data
     * @return Response
     */
    protected function request($method, $routeId, array $routeParams = array(), array $data = array())
    {
        $client = static::createClient();
        $client->request($method, $this->getRoute($routeId, $routeParams), $data, array());
        return $client->getResponse();
    }

    protected function getContainer()
    {
        $client = static::createClient();
        return $client->getContainer();
    }

    protected function getRoute($routeId, array $parameters = array())
    {
        $client = static::createClient();
        $loginRoute = $client->getContainer()->get('router')->generate($routeId, $parameters);
        return $loginRoute;
    }

    # ASSERTIONS

    final protected function assertResponseJson(Response $response)
    {
        $this->assertTrue($response->headers->has('content_type'));
        $this->assertEquals('application/json', $response->headers->get('content_type'));
    }

    final protected function assertSuccessfulResponseWithManyItems(Response $response, array $dataArrayKeys = array(), array $metaDataArrayKeys = array(), $num=null)
    {
        $this->assertTrue($response->isSuccessful());
        $this->assertResponseJson($response);
        $data = json_decode($response->getContent(),true);
        $this->assertNotNull($data); # Json is decoded successfully
        $this->assertGreaterThan(0, count($data['data']));

        if($num !== null)
        {
            $this->assertEquals($num, count($data['data']));
        }

        foreach($dataArrayKeys as $key)
        {
            $this->assertArrayHasKey($key, array_values($data['data'])[0]);
        }

        foreach($metaDataArrayKeys as $key)
        {
            $this->assertArrayHasKey($key, array_values($data)[0]);
        }
    }

    final protected function assertSuccessfulResponseWithOneItem(Response $response, array $dataArrayKeys = array(), array $metaDataArrayKeys = array())
    {
        $this->assertTrue($response->isSuccessful());
        $this->assertResponseJson($response);
        $data = json_decode($response->getContent(),true);
        $this->assertNotNull($data); # Json is decoded successfully

        foreach($dataArrayKeys as $key)
        {
            $this->assertArrayHasKey($key, $data['data']);
        }

        foreach($metaDataArrayKeys as $key)
        {
            $this->assertArrayHasKey($key, $data);
        }
    }

    final protected function assertSuccessfulResponse(Response $response)
    {
        $this->assertTrue($response->isSuccessful());
    }

    final protected function assertResourceNotFoundResponse(Response $response)
    {
        $this->assertTrue($response->isNotFound());
    }

    final protected function assertForbiddenResponse(Response $response)
    {
        $this->assertTrue($response->isForbidden());
    }

    final protected function assertCreatedResponse(Response $response)
    {
        $this->assertTrue($response->getStatusCode() === Response::HTTP_CREATED);
    }

    final protected function assertContentType(Response $response, $contentType)
    {
        $this->assertContains($contentType, $response->headers->get('Content-Type'));
    }

    # DECODER

    protected function decode(Response $response, $extractKey = null)
    {
        $this->assertResponseJson($response);

        $data = json_decode($response->getContent(),true)['data'];

        if($extractKey === null)
        {
            return $data;
        }
        else
        {
            if(isset($data[$extractKey]))
            {
                return $data[$extractKey];
            }

            $keys = array();

            foreach($data as $d)
            {
                $keys[] = $d[$extractKey];
            }

            return $keys;
        }
    }
} 