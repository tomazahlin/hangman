<?php
/**
 * Created by PhpStorm.
 * User: Tomaz
 * Date: 7.3.2014
 * Time: 15:48
 */

namespace Qandidate\Bundle\ToolBundle\Event\Listener;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof EntityNotFoundException) {
            $response = new JsonResponse(null, 404);
            $event->setResponse($response);
        }
    }
}